# Wikipedia Articles

## Art

### Comics

[Glossary of comics terminology](https://en.wikipedia.org/wiki/Glossary_of_comics_terminology)

[The Lexicon of Comicana](https://en.wikipedia.org/wiki/The_Lexicon_of_Comicana) - a 1980 book by the American cartoonist Mort Walker. It was intended as a tongue-in-cheek look at the devices used by cartoonists. In it, Walker invented an international set of symbols called symbolia after researching cartoons around the world.

### Graphic Design

[Greeking](https://en.wikipedia.org/wiki/Greeking) - a style of displaying or rendering text or symbols, not always from the Greek alphabet. Greeking obscures portions of a work for the purpose of either emphasizing form over details or displaying placeholders for unavailable content.

### Music

[Backmasking](https://en.wikipedia.org/wiki/Backmasking) - a recording technique in which a sound or message is recorded backward onto a track that is meant to be played forward.

[Crab canon](https://en.wikipedia.org/wiki/Crab_canon) - an arrangement of two musical lines that are complementary and backward, similar to a palindrome.

[Earworm](https://en.wikipedia.org/wiki/Earworm) - a catchy piece of music that continually repeats through a person's mind after it is no longer playing.

[Mirror canon](https://en.wikipedia.org/wiki/Mirror_canon) - a type of canon which involves the leading voice being played alongside its own inversion (i.e. upside-down).

[Shepard tone](https://en.wikipedia.org/wiki/Shepard_tone) - is a sound consisting of a superposition of sine waves separated by octaves. This creates the auditory illusion of a tone that continually ascends or descends in pitch, yet which ultimately seems to get no higher or lower.

### Visual Art

[Animal-made art](https://en.wikipedia.org/wiki/Animal-made_art) - art created by an animal. Animal-made works of art have been created by apes, elephants, cetacea, reptiles, and bowerbirds, among other species.

[Lost artworks](https://en.wikipedia.org/wiki/Lost_artworks) - original pieces of art that credible sources indicate once existed but that cannot be accounted for in museums or private collections or are known to have been destroyed deliberately or accidentally, or neglected through ignorance and lack of connoisseurship.

[Outsider art](https://en.wikipedia.org/wiki/Outsider_art) - art by self-taught or naïve art makers.

## Computer Science

### Attacks

[Billion laughs attack](https://en.wikipedia.org/wiki/Billion_laughs_attack) - a type of denial-of-service (DoS) attack which is aimed at parsers of XML documents.

[Email bomb](https://en.wikipedia.org/wiki/Email_bomb) - a form of net abuse consisting of sending huge volumes of email to an address in an attempt to overflow the mailbox or overwhelm the server where the email address is hosted in a denial-of-service attack (DoS attack).

[Fork bomb](https://en.wikipedia.org/wiki/Fork_bomb) - a denial-of-service attack wherein a process continually replicates itself to deplete available system resources, slowing down or crashing the system due to resource starvation.

[Google hacking](https://en.wikipedia.org/wiki/Google_hacking) - a computer hacking technique that uses Google Search and other Google applications to find security holes in the configuration and computer code that websites use.

[Pass the hash](https://en.wikipedia.org/wiki/Pass_the_hash) - a hacking technique that allows an attacker to authenticate to a remote server or service by using the underlying NTLM or LanMan hash of a user's password, instead of requiring the associated plaintext password as is normally the case.

[R-U-Dead-Yet Attack](https://en.wikipedia.org/wiki/R-U-Dead-Yet) - Low and slow DoS attack

[Side-channel attack](https://en.wikipedia.org/wiki/Side-channel_attack) - any attack based on information gained from the implementation of a computer system, rather than weaknesses in the implemented algorithm itself (e.g. cryptanalysis and software bugs).

[Slowloris](https://en.wikipedia.org/wiki/Slowloris_%28computer_security%29) - Another slow DoS attack

[Watering hole attack](https://en.wikipedia.org/wiki/Watering_hole_attack) - a computer attack strategy, in which the victim is a particular group (organization, industry, or region). In this attack, the attacker guesses or observes which websites the group often uses and infects one or more of them with malware. Eventually, some member of the targeted group becomes infected.

[Zip bomb](https://en.wikipedia.org/wiki/Zip_bomb) - a malicious archive file designed to crash or render useless the program or system reading it. It is often employed to disable antivirus software, in order to create an opening for more traditional viruses.

### Bugs

[MissingNo.](https://en.wikipedia.org/wiki/MissingNo.) - short for Missing Number, is an unofficial Pokémon species found in the video games Pokémon Red and Blue. Due to the programming of certain in-game events, players can encounter MissingNo. via a glitch.

[Mojibake](https://en.wikipedia.org/wiki/Mojibake) - the garbled text that is the result of text being decoded using an unintended character encoding.

[Scunthorpe Problem](https://en.wikipedia.org/wiki/Scunthorpe_problem) - The Scunthorpe problem is the blocking of websites, e-mails, forum posts or search results by a spam filter or search engine because their text contains a string of letters that appear to have an obscene or unacceptable meaning.

[Time-of-check to time-of-use](https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use) - time-of-check to time-of-use (TOCTOU, TOCTTOU or TOC/TOU) is a class of software bugs caused by a race condition involving the checking of the state of a part of a system (such as a security credential) and the use of the results of that check.

[Year 2000 Problem](https://en.wikipedia.org/wiki/Year_2000_problem) - A class of computer bugs related to the formatting and storage of calendar data for dates beginning in the year 2000.

### Cryptography

[Cryptographic attacks](https://en.wikipedia.org/wiki/Category:Cryptographic_attacks) - a method for circumventing the security of a cryptographic system by finding a weakness in a code, cipher, cryptographic protocol or key management scheme. This process is also called "cryptanalysis".

[Commitment scheme](https://en.wikipedia.org/wiki/Commitment_scheme) - a cryptographic primitive that allows one to commit to a chosen value (or chosen statement) while keeping it hidden to others, with the ability to reveal the committed value later. Commitment schemes are designed so that a party cannot change the value or statement after they have committed to it: that is, commitment schemes are binding.

[Coppersmith's attack](https://en.wikipedia.org/wiki/Coppersmith%27s_attack) - a class of cryptographic attacks on the public-key cryptosystem RSA based on the Coppersmith method.

[Coppersmith method](https://en.wikipedia.org/wiki/Coppersmith_method) - a method to find small integer zeroes of univariate or bivariate polynomials modulo a given integer.

[Differential Cryptanalysis](https://en.wikipedia.org/wiki/Differential_cryptanalysis) - Differential cryptanalysis is a general form of cryptanalysis applicable primarily to block ciphers, but also to stream ciphers and cryptographic hash functions.

[Diffie-Hellman key exchange](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange) - a method of securely exchanging cryptographic keys over a public channel.

[Entropy (information theory)](https://en.wikipedia.org/wiki/Entropy_(information_theory)) - is the average rate at which information is produced by a stochastic source of data.

[Identicon](https://en.wikipedia.org/wiki/Identicon) - An Identicon is a visual representation of a hash value, usually of an IP address, that serves to identify a user of a computer system as a form of avatar while protecting the users' privacy.

[Key Schedule](https://en.wikipedia.org/wiki/Key_schedule) - In cryptography, the so-called product ciphers are a certain kind of cipher, where the (de-)ciphering of data is typically done as an iteration of rounds.

[Linux Unified Key Setup](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup) - disk encryption specification created by Clemens Fruhwirth in 2004 and was originally intended for Linux.

[Merkle Tree](https://en.wikipedia.org/wiki/Merkle_tree) - a hash tree or Merkle tree is a tree in which every leaf node is labelled with the hash of a data block, and every non-leaf node is labelled with the cryptographic hash of the labels of its child nodes.

[Nihilist cipher](https://en.wikipedia.org/wiki/Nihilist_cipher) - manually operated symmetric encryption cipher, originally used by Russian Nihilists in the 1880s to organize terrorism against the tsarist regime.

[Post-quantum Cryptography](https://en.wikipedia.org/wiki/Post-quantum_cryptography) -  Cryptographic algorithms (usually public-key algorithms) that are thought to be secure against an attack by a quantum computer

[Rail fence cipher](https://en.wikipedia.org/wiki/Rail_fence_cipher) - a form of transposition cipher. It derives its name from the way in which it is encoded.

[Related-key Attack](https://en.wikipedia.org/wiki/Related-key_attack) - In cryptography, a related-key attack is any form of cryptanalysis where the attacker can observe the operation of a cipher under several different keys whose values are initially unknown, but where some mathematical relationship connecting the keys is known to the attacker.

[Slide Attack](https://en.wikipedia.org/wiki/Slide_attack) - The slide attack is a form of cryptanalysis designed to deal with the prevailing idea that even weak ciphers can become very strong by increasing the number of rounds, which can ward off a differential attack.

### Data Structures

[Rope](https://en.wikipedia.org/wiki/Rope_%28data_structure%29) - a data structure composed of smaller strings that is used to efficiently store and manipulate a very long string. For example, a text editing program may use a rope to represent the text being edited, so that operations such as insertion, deletion, and random access can be done efficiently.

### Encoding

[Punycode](https://en.wikipedia.org/wiki/Punycode) - a representation of Unicode with the limited ASCII character subset used for Internet host names.

### Files

[Run commands](https://en.wikipedia.org/wiki/Run_commands) - In the context of Unix-like systems, the term rc stands for the phrase "run commands". It is used for any file that contains startup information for a command.

### Graphics

[Box-drawing character](https://en.wikipedia.org/wiki/Box-drawing_character) - a form of semigraphics widely used in text user interfaces to draw various geometric frames and boxes. Box-drawing characters typically only work well with monospaced fonts.

[Greeble](https://en.wikipedia.org/wiki/Greeble) - or nurnie is a fine detailing added to the surface of a larger object that makes it appear more complex, and therefore more visually interesting.

[Stanford Bunny](https://en.wikipedia.org/wiki/Stanford_bunny) - Standard for scanning 3d objects

[Utah Teapot](https://en.wikipedia.org/wiki/Utah_teapot) - Standard 3D test model

[Z-Fighting](https://en.wikipedia.org/wiki/Z-fighting) - When 2 planes are in the same position and 'mesh' together.

### Image Processing

[Lenna](https://en.wikipedia.org/wiki/Lenna) - Standard test image for image processing

### Logic

[Angelic non-determinism](https://en.wikipedia.org/wiki/Angelic_non-determinism) - the execution of a non-deterministic program where all choices that are made favor termination of the program.

[Billiard-ball computer](https://en.wikipedia.org/wiki/Billiard-ball_computer) - an idealized model of a reversible mechanical computer based on Newtonian dynamics.

[Demonic non-determinism](https://en.wikipedia.org/wiki/Demonic_non-determinism) - describes the execution of a non-deterministic program where all choices that are made favour non-termination.

### Machines

[Busy beaver](https://en.wikipedia.org/wiki/Busy_beaver) - The busy beaver game consists of designing a halting, binary-alphabet Turing machine which writes the most 1s on the tape, using only a limited set of states. The rules for the 2-state game are as follows: 1. the machine must have two states in addition to the halting state, and 2. the tape starts with 0s only. As the player, you should conceive each state aiming for the maximum output of 1s on the tape while making sure the machine will halt eventually.

[Turmite](https://en.wikipedia.org/wiki/Turmite) - a Turing machine which has an orientation as well as a current state and a "tape" that consists of an infinite two-dimensional grid of cells. The terms ant and vant are also used.

### Networking

[Hyper Text Coffee Pot Control Protocol](https://en.wikipedia.org/wiki/Hyper_Text_Coffee_Pot_Control_Protocol) - a facetious communication protocol for controlling, monitoring, and diagnosing coffee pots.

[Internet mail protocols](https://en.wikipedia.org/wiki/Category:Internet_mail_protocols)

[List of TCP/UDP Ports](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)

[Network Telescope](https://en.wikipedia.org/wiki/Network_telescope) - An Internet system that allows one to observe different large-scale events taking place on the Internet

[QUIC](https://en.wikipedia.org/wiki/QUIC) - an experimental general-purpose transport layer network protocol.

[SPDY](https://en.wikipedia.org/wiki/SPDY) -  deprecated open-specification networking protocol that was developed primarily at Google for transporting web content.

### Operating Systems

[TempleOS](https://en.m.wikipedia.org/wiki/TempleOS) - Handwritten operating system written in Holy C.

### Programming

[Big ball of mud](https://en.wikipedia.org/wiki/Big_ball_of_mud) - a software system that lacks a perceivable architecture.

[Browser war](https://en.wikipedia.org/wiki/Browser_wars) - ompetition for dominance in the usage share of web browsers. The "First Browser War" during the late 1990s pitted Microsoft's Internet Explorer against Netscape's Navigator.

[Code smell](https://en.wikipedia.org/wiki/Code_smell) - any characteristic in the source code of a program that possibly indicates a deeper problem.

[Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) -  a software design paradigm used by software frameworks that attempts to decrease the number of decisions that a developer using the framework is required to make without necessarily losing flexibility.

[Creeping elegance](https://en.wikipedia.org/wiki/Creeping_elegance) - the tendency of programmers to disproportionately emphasize elegance in software at the expense of other requirements such as functionality, shipping schedule, and usability.

[Data Clump](https://en.wikipedia.org/wiki/Data_Clump_(Code_Smell)) - a name given to any group of variables which are passed around together (in a clump) throughout various parts of the program.

[Dependency hell](https://en.wikipedia.org/wiki/Dependency_hell) - a colloquial term for the frustration of some software users who have installed software packages which have dependencies on specific versions of other software packages.

[Design smell](https://en.wikipedia.org/wiki/Design_smell) - "structures in the design that indicate violation of fundamental design principles and negatively impact design quality".

[Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) - when an organization uses its own product.

[DLL Hell](https://en.wikipedia.org/wiki/DLL_Hell) - a term for the complications which arise when one works with dynamic-link libraries (DLLs) used with Microsoft Windows operating systems, particularly legacy 16-bit editions, which all run in a single memory space.

[Editor war](https://en.wikipedia.org/wiki/Editor_war) - the common name for the rivalry between users of the Emacs and vi (usually Vim) text editors.

[Feature creep](https://en.wikipedia.org/wiki/Feature_creep) - the excessive ongoing expansion or addition of new features in a product, especially in computer software, videogames and consumer and business electronics.

[God object](https://en.wikipedia.org/wiki/God_object) - an object that knows too much or does too much. The God object is an example of an anti-pattern.

[Greenspun's tenth rule](https://en.wikipedia.org/wiki/Greenspun%27s_tenth_rule) - Any sufficiently complicated C or Fortran program contains an ad-hoc, informally-specified, bug-ridden, slow implementation of half of Common Lisp.

[International Obfuscated C Code Contest](https://en.wikipedia.org/wiki/International_Obfuscated_C_Code_Contest) - a computer programming contest for the most creatively obfuscated C code.

[JAR hell](https://en.wikipedia.org/wiki/Java_Classloader#JAR_hell) - a term similar to DLL hell used to describe all the various ways in which the classloading process can end up not working.

[Just another Perl hacker](https://en.wikipedia.org/wiki/Just_another_Perl_hacker) - a Perl program which prints "Just another Perl hacker," (the comma is canonical but is occasionally omitted). Short JAPH programs are often used as signatures in online forums, or as T-shirt designs.

[Kolmogorov complexity](https://en.wikipedia.org/wiki/Kolmogorov_complexity) - the length of the shortest computer program (in a predetermined programming language) that produces the object as output.

[Lasagna code](https://en.wikipedia.org/wiki/Spaghetti_code#Lasagna_code) - code whose layers are so complicated and intertwined that making a change in one layer would necessitate changes in all other layers.

[Polymorphic code](https://en.wikipedia.org/wiki/Polymorphic_code) - code that uses a polymorphic engine to mutate while keeping the original algorithm intact. That is, the code changes itself each time it runs, but the function of the code (its semantics) will not change at all.

[Quine](https://en.wikipedia.org/wiki/Quine_(computing)) - A self replicating program.

[Ravioli code](https://en.wikipedia.org/wiki/Spaghetti_code#Ravioli_code) - code that comprises well-structured classes that are easy to understand in isolation, but difficult to understand as a whole.

[Rubber duck debugging](https://en.wikipedia.org/wiki/Rubber_duck_debugging) - Talking out your program to find bugs.

[Shotgun debugging](https://en.wikipedia.org/wiki/Shotgun_debugging) - Making haphazard or diverse changes to software in the hope that a bug will be perturbed out of existence.

[Shotgun surgery](https://en.wikipedia.org/wiki/Shotgun_surgery) - an antipattern in software development and occurs where a developer adds features to an application codebase which span a multiplicity of implementors or implementations in a single change.

[Software bloat](https://en.wikipedia.org/wiki/Software_bloat) - a process whereby successive versions of a computer program become perceptibly slower, use more memory, disk space or processing power, or have higher hardware requirements than the previous version—whilst making only dubious user-perceptible improvements or suffering from feature creep.

[Software brittleness](https://en.wikipedia.org/wiki/Software_brittleness) - the increased difficulty in fixing older software that may appear reliable, but fails badly when presented with unusual data or altered in a seemingly minor way.

[Software rot](https://en.wikipedia.org/wiki/Software_rot) - a slow deterioration of software performance over time or its diminishing responsiveness that will eventually lead to software becoming faulty, unusable, or otherwise called "legacy" and in need of upgrade.

[Spaghetti code](https://en.wikipedia.org/wiki/Spaghetti_code) -  pejorative phrase for unstructured and difficult-to-maintain source code. Spaghetti code can be caused by several factors, such as volatile project requirements, lack of programming style rules, and insufficient ability or experience.

[Stovepipe](https://en.wikipedia.org/wiki/Stovepipe_system) - a pejorative term for a system that has the potential to share data or functionality with other systems but which does not do so.

[Turing tarpit](https://en.wikipedia.org/wiki/Turing_tarpit) - any programming language or computer interface that allows for flexibility in function but is difficult to learn and use because it offers little or no support for common tasks.

[Underhanded C Contest](https://en.wikipedia.org/wiki/Underhanded_C_Contest) - a programming contest to turn out code that is malicious, but passes a rigorous inspection, and looks like an honest mistake even if discovered.

[Wirth's law](https://en.wikipedia.org/wiki/Wirth%27s_law) - an adage on computer performance which states that software is getting slower more rapidly than hardware becomes faster.

[Write-only language](https://en.wikipedia.org/wiki/Write-only_language) - a pejorative term for a programming language alleged to have syntax or semantics sufficiently dense and bizarre that any routine of significant size is too difficult to understand by other programmers and cannot be safely edited.

### Security

[Dancing pigs](https://en.wikipedia.org/wiki/Dancing_pigs) - Given a choice between dancing pigs and security, users will pick dancing pigs every time.

[Google hacking](https://en.wikipedia.org/wiki/Google_hacking) -  also named Google Dorking, is a computer hacking technique that uses Google Search and other Google applications to find security holes in the configuration and computer code that websites use.

## Design

[Anti urination devices in Norwich](https://en.wikipedia.org/wiki/Anti_urination_devices_in_Norwich) - a form of hostile architecture installed in Norwich and the surrounding area in the late 19th century to discourage public urination.

[Camden Bench](https://en.wikipedia.org/wiki/Camden_bench) - It is designed specifically to influence the behaviour of the public by restricting undesirable behaviour, a principle known as hostile architecture, and instead be usable only as a bench.

[Defensive Design](https://en.wikipedia.org/wiki/Defensive_design) -  the practice of planning for contingencies in the design stage of a project or undertaking.

[Hostile architecture](https://en.wikipedia.org/wiki/Hostile_architecture) - an urban design trend in which public spaces are constructed or altered to discourage people from using them in a way not intended by the owner.

## Economics

[Game theory](https://en.wikipedia.org/wiki/Game_theory) - the study of mathematical models of strategic interaction between rational decision-makers

[Local Currency](https://en.wikipedia.org/wiki/Local_currency) - In economics, a local currency is a currency that can be spent in a particular geographical locality at participating organisations.

[Monopsony](https://en.wikipedia.org/wiki/Monopsony) - a market structure in which a single buyer substantially controls the market as the major purchaser of goods and services offered by many would-be sellers.

## History & Anthropology

[Abuwtiyuw](https://en.wikipedia.org/wiki/Abuwtiyuw) - The Egyptian dog Abuwtiyuw was one of the earliest documented domestic animals whose name is known.

[Acoustic Kitty](https://en.wikipedia.org/wiki/Acoustic_Kitty) - Acoustic Kitty was a CIA project launched by the Central Intelligence Agency Directorate of Science & Technology, which in the 1960s intended to use cats to spy on the Kremlin and Soviet embassies.

[Cargo Cult](https://en.wikipedia.org/wiki/Cargo_cult) - A belief system among members of a relatively undeveloped society in which adherents practice superstitious rituals hoping to bring modern goods supplied by a more technologically advanced society.

[Flitch of bacon custom](https://en.wikipedia.org/wiki/Flitch_of_bacon_custom) - The awarding of a flitch of bacon to married couples who can swear to not having regretted their marriage for a year and a day is an old tradition, the remnants of which still survive in some pockets in England.

[Gay Nineties](https://en.wikipedia.org/wiki/Gay_Nineties) - an American nostalgic term and a periodization of the history of the United States referring to the decade of the 1890s.

[Immurement](https://en.wikipedia.org/wiki/Immurement) - is a form of imprisonment, usually until death, in which a person is placed within an enclosed space with no exits.

[Murder Hole](https://en.wikipedia.org/wiki/Murder_hole) - a hole in the ceiling of a gateway or passageway in a fortification through which the defenders could fire, throw or pour harmful substances or objects, such as rocks, arrows, scalding water, hot sand, quicklime, tar, or boiling oil, down on attackers.

[Naruto run](https://en.wikipedia.org/wiki/Naruto#Naruto_run) - a running style based on the way the characters run leaning forward with their arms behind their backs.

[Pepsi Fruit Juice Flood](https://en.wikipedia.org/wiki/Pepsi_Fruit_Juice_Flood) - a flood of 176,000 barrels (28 million litres; 7.4 million US gallons) of fruit and vegetable juices into the streets of Lebedyan, Russia and the Don River, caused by the collapse of a PepsiCo warehouse.

[Storm Area 51](https://en.wikipedia.org/wiki/Storm_Area_51) - an American Facebook event that took place on September 20, 2019, at Area 51, a United States Air Force (USAF) facility within the Nevada Test and Training Range, to raid the site in a search for extraterrestrial life.

[Sword of Damocles](https://en.wikipedia.org/wiki/Damocles) - King who hung a sword from a string above his head to represent the constant threat involved in ruling

[Toilet-related injuries and deaths](https://en.wikipedia.org/wiki/Toilet-related_injuries_and_deaths)

### Laws

[Statue forbidding Bearing of Armour](https://en.wikipedia.org/wiki/Statute_forbidding_Bearing_of_Armour) - was enacted in 1313 during the reign of Edward II of England. It decrees "that in all Parliaments, Treatises and other Assemblies, which should be made in the Realm of England for ever, that every Man shall come without all Force and Armour".

### People

[Digby Tatham-Warter](https://en.wikipedia.org/wiki/Digby_Tatham-Warter) - Digby got to him and said "Don't worry about the bullets, I've got an umbrella". He then escorted the chaplain across the street under his umbrella. When he returned to the front line, one of his fellow officers said about his umbrella that "that thing won't do you any good", to which Digby replied "Oh my goodness Pat, but what if it rains?"

### Wars

[Kettle War](https://en.wikipedia.org/wiki/Kettle_War) - The Kettle War was a military confrontation between the troops of the Holy Roman Empire and the Republic of the Seven Netherlands on 8 October 1784. It was named the Kettle War because the only shot fired hit a soup kettle.

## Internet

[Chum Box](https://en.wikipedia.org/wiki/Chum_box) - A chum box (or chumbucket) is a form of online advertising that uses a grid of thumbnails and captions to drive traffic to other sites and webpages.

[Eternal September](https://en.wikipedia.org/wiki/Eternal_September) - is Usenet slang for a period beginning in September 1993, the month that Internet service provider America Online (AOL) began offering Usenet access to its many users, overwhelming the existing culture for online forums.

[Monkey selfie copyright dispute](https://en.wikipedia.org/wiki/Monkey_selfie_copyright_dispute) - a series of disputes about the copyright status of selfies taken by Celebes crested macaques using equipment belonging to the British nature photographer David Slater.

## Language

### Grammar

[Adjective Order](https://en.wikipedia.org/wiki/Adjective#Order) - In many languages, attributive adjectives usually occur in a specific order. In general, the adjective order in English can be summarised as: opinion, size, age or shape, colour, origin, material, purpose. This sequence (with age preceding shape) is sometimes referred to by the mnemonic OSASCOMP.

### Idioms

[Hobson's Choice](https://en.wikipedia.org/wiki/Hobson's_choice) - a free choice in which only one thing is offered. Because a person may refuse to accept what is offered, the two options are taking it or taking nothing. In other words, one may "take it or leave it".

[Morton's Fork](https://en.wikipedia.org/wiki/Morton's_fork) - a type of false dilemma in which contradictory observations lead to the same conclusion. It is said to have originated with the collecting of taxes by John Morton.

### Languages

[Avoidance speech](https://en.wikipedia.org/wiki/Avoidance_speech) - a group of sociolinguistic phenomena in which a special restricted speech style must be used in the presence of or in reference to certain relatives.

[Constructed Languages](https://en.wikipedia.org/wiki/Constructed_language) - A constructed language (sometimes called a conlang) is a language whose phonology, grammar, and vocabulary are, instead of having developed naturally, consciously devised for communication between intelligent beings, most commonly for use by humanoids.

[Dog Latin](https://en.wikipedia.org/wiki/Dog_Latin) - the creation of a phrase or jargon in imitation of Latin, often by "translating" English words (or those of other languages) into Latin by conjugating or declining them as if they were Latin words.

[False friends](https://en.wikipedia.org/wiki/False_friend) - words in different languages that look or sound similar, but differ significantly in meaning.

[Latin obscenity](https://en.wikipedia.org/wiki/Latin_obscenity) - the profane, indecent, or impolite vocabulary of Latin, and its uses. Words deemed obscene were described as obsc(a)ena (obscene, lewd, unfit for public use), or improba (improper, in poor taste, undignified).

[Linguistic relativity](https://en.wikipedia.org/wiki/Linguistic_relativity) - the structure of a language affects its speakers' world view or cognition.

[Pandanus language](https://en.wikipedia.org/wiki/Pandanus_language) - an elaborate avoidance language among several of the peoples of the eastern New Guinea Highlands, used when collecting Pandanus nuts.

[Stylometry](https://en.wikipedia.org/wiki/Stylometry) - the application of the study of linguistic style, usually to written language, but it has successfully been applied to music and to fine-art paintings as well.

[Theives' cant](https://en.wikipedia.org/wiki/Thieves'_cant) - a secret language (a cant or cryptolect) which was formerly used by thieves, beggars and hustlers of various kinds in Great Britain and to a lesser extent in other English-speaking countries.

### Letters and Glyphs

[Asemic writing](https://en.wikipedia.org/wiki/Asemic_writing) - a wordless open semantic form of writing.

[Deseret Alphabet](https://en.wikipedia.org/wiki/Deseret_alphabet) - a phonemic English-language spelling reform developed between 1847 and 1854 by the board of regents of the University of Deseret under the leadership of Brigham Young, the second president of the Church of Jesus Christ of Latter-day Saints.

[Multiocular O](https://en.wikipedia.org/wiki/Multiocular_O) - ꙮ

### Poems

[l(a](https://en.wikipedia.org/wiki/L(a)) - a poem by E. E. Cummings. It is the first poem in his 1958 collection 95 Poems.

### Punctuation

[Interrobang](https://en.wikipedia.org/wiki/Interrobang) - a punctuation mark used in various written languages and intended to combine the functions of the question mark, or interrogative point, and the exclamation mark, or exclamation point, known in the jargon of printers and programmers as a "bang".

[Irony punctuation](https://en.wikipedia.org/wiki/Irony_punctuation) - any proposed form of notation used to denote irony or sarcasm in text.

### Weird words, phrases, and sentences

[Alternatives to the Ten Commandments](https://en.wikipedia.org/wiki/Alternatives_to_the_Ten_Commandments) - Several alternatives to the Ten Commandments have been promulgated by different persons and groups, which intended to improve on the lists of laws known as the Ten Commandments that appear in the Bible.

[Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo](https://en.wikipedia.org/wiki/Buffalo_buffalo_Buffalo_buffalo_buffalo_buffalo_Buffalo_buffalo) - is a grammatically correct sentence in American English, often presented as an example of how homonyms and homophones can be used to create complicated linguistic constructs through lexical ambiguity.

[Colorless green ideas sleep furiously](https://en.wikipedia.org/wiki/Colorless_green_ideas_sleep_furiously) - a sentence composed by Noam Chomsky in his 1957 book Syntactic Structures as an example of a sentence that is grammatically correct, but semantically nonsensical.

[Dord](https://en.wikipedia.org/wiki/Dord) - a dictionary error in lexicography. On July 31, 1931, Austin M. Patterson, the dictionary's chemistry editor, sent in a slip reading "D or d, cont./density." This was intended to add "density" to the existing list of words that the letter "D" can abbreviate. The phrase "D or d" was misinterpreted as a single, run-together word: Dord.

[Ghoti](https://en.wikipedia.org/wiki/Ghoti) - a creative respelling of the word fish, used to illustrate irregularities in English spelling and pronunciation.

[Satiric misspelling](https://en.wikipedia.org/wiki/Satiric_misspelling) - an intentional misspelling of a word, phrase or name for a rhetorical purpose. This is often done by replacing a letter with another letter (for example, k replacing c), or symbol (for example, $ replacing s, @ replacing a, or ¢ replacing c).

### Words

[-graphy](https://en.wikipedia.org/wiki/-graphy) - The English suffix -graphy means either "writing" or a "field of study"

[Apherisis](https://en.wikipedia.org/wiki/Apheresis_(linguistics)) - the loss of one or more sounds from the beginning of a word, especially the loss of an unstressed vowel, thus producing a new form called an aphetism.

[Auto-antonym](https://en.wikipedia.org/wiki/Auto-antonym) - also called a contronym, contranym or Janus word, is a word with multiple meanings (senses) of which one is the reverse of another. For example, the word cleave can mean "to cut apart" or "to bind together".

[Autological Word](https://en.wikipedia.org/wiki/Autological_word) - a word that expresses a property that it also possesses

[Demonym](https://en.wikipedia.org/wiki/Demonym) - is a word that identifies residents or natives of a particular place and is derived from the name of the place.

[Eggcorn](https://en.wikipedia.org/wiki/Eggcorn) - An idiosyncratic substitution of a word or phrase for a word or words that sound similar or identical in the speaker's dialect (sometimes called oronyms).

[Etaoin shrdlu](https://en.wikipedia.org/wiki/Etaoin_shrdlu) - a nonsense phrase that sometimes appeared in print in the days of "hot type" publishing because of a custom of type-casting machine operators.

[Fumblerules](https://en.wikipedia.org/wiki/Fumblerules) - A fumblerule is a rule of language or linguistic style, humorously written in such a way that it breaks this rule.

[Ghost word](https://en.wikipedia.org/wiki/Ghost_word) - a word published in a dictionary or similarly authoritative reference work, having rarely, if ever, been used in practice, and hitherto having been meaningless.

[Hapax legomenon](https://en.wikipedia.org/wiki/Hapax_legomenon) - a word that only occurs once within a certain context.

[Headlinese](https://en.wikipedia.org/wiki/Headlinese) -  an abbreviated form of news writing style used in newspaper headlines.

[Heterogram](https://en.wikipedia.org/wiki/Heterogram_(literature)) - a word, phrase, or sentence in which no letter of the alphabet occurs more than once.

[Irony](https://en.wikipedia.org/wiki/Irony) - a rhetorical device, literary technique, or event in which what appears, on the surface, to be the case, differs radically from what is actually the case.

[Isogram](https://en.wikipedia.org/wiki/Isogram) - a logological term for a word or phrase without a repeating letter.

[Lorem Ipsum](https://en.wikipedia.org/wiki/Lorem_ipsum) - a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content (also called greeking).

[Mondegreen](https://en.wikipedia.org/wiki/Mondegreen) - Mishearing of a phrase that results in near-homophony, in a way that gives it a new meaning.

[Palindrome](https://en.wikipedia.org/wiki/Palindrome) - a word, number, phrase, or other sequence of characters which reads the same backward as forward, such as madam or racecar or the number 10801.

[Panalphabetic window](https://en.wikipedia.org/wiki/Panalphabetic_window) - a stretch of text that contains all the letters of the alphabet in order. It is a special type of pangram or pangrammatic window.

[Pangram](https://en.wikipedia.org/wiki/Pangram) - a sentence using every letter of a given alphabet at least once.

[Pangrammatic lipogram](https://en.wikipedia.org/wiki/Lipogram#Pangrammatic_lipogram) - uses every letter of the alphabet except one.

[Pangrammatic window](https://en.wikipedia.org/wiki/Pangrammatic_window) - a stretch of naturally occurring text that contains all the letters in the alphabet.

[Phonetic reversal](https://en.wikipedia.org/wiki/Phonetic_reversal) - the process of reversing the phonemes or phones of a word or phrase.

[Placeholder name](https://en.wikipedia.org/wiki/Placeholder_name) -  words that can refer to objects or people whose names are temporarily forgotten, irrelevant, or unknown in the context in which they are being discussed.

[RAS Syndrome](https://en.wikipedia.org/wiki/RAS_syndrome) - is the use of one or more of the words that make up an acronym (or other initialism) in conjunction with the abbreviated form. This means, in effect, repeating one or more words from the acronym. Two common examples are "PIN number"/ "VIN number" (the "N" in PIN and VIN stands for "number") and "ATM machine" (the "M" in ATM stands for "machine").

[Sniglet](https://en.wikipedia.org/wiki/Sniglet) - an often humorous word made up to describe something for which no dictionary word exists.

[Weasel word](https://en.wikipedia.org/wiki/Weasel_word) - an informal term for words and phrases such as "researchers believe" and "most people think" which make arguments appear specific or meaningful, even though these terms are at best ambiguous and vague.

[Yak Shaving](https://en.wiktionary.org/wiki/yak_shaving) - Any apparently useless activity which, by allowing you to overcome intermediate difficulties, allows you to solve a larger problem.

## Math

### Algebra

[1 + 2 + 3 + 4...](https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF)

[Digital root](https://en.wikipedia.org/wiki/Digital_root) - a non-negative integer is the (single digit) value obtained by an iterative process of summing digits, on each iteration using the result from the previous iteration to compute a digit sum.

[Magma](https://en.wikipedia.org/wiki/Magma_(algebra)) - In abstract algebra, a magma (or groupoid) is a basic kind of algebraic structure. Specifically, a magma consists of a set equipped with a single binary operation. The binary operation must be closed by definition but no other properties are imposed.

[Modular arithmetic](https://en.wikipedia.org/wiki/Modular_arithmetic) - a system of arithmetic for integers, where numbers "wrap around" upon reaching a certain value—the modulus (plural moduli).

### Functions

[Arity](https://en.wikipedia.org/wiki/Arity) - In logic, mathematics, and computer science, the arity of a function or operation is the number of arguments or operands that the function takes.

[Binary Operation](https://en.wikipedia.org/wiki/Binary_operation) - In mathematics, a binary operation on a set is a calculation that combines two elements of the set (called operands) to produce another element of the set.

[Linear congruential generator](https://en.wikipedia.org/wiki/Linear_congruential_generator) - an algorithm that yields a sequence of pseudo-randomized numbers calculated with a discontinuous piecewise linear equation

[Weierstrass Function](https://en.wikipedia.org/wiki/Weierstrass_function) - The function has the property of being continuous everywhere but differentiable nowhere.

### Geometry

[Constructive Solid Geometry](https://en.wikipedia.org/wiki/Constructive_solid_geometry) - Constructive solid geometry allows a modeler to create a complex surface or object by using Boolean operators to combine simpler objects.

[Cox-Zucker machine](https://en.wikipedia.org/wiki/Cox%E2%80%93Zucker_machine) - n algorithm created by David A. Cox and Steven Zucker. This algorithm determines if a given set of sections provides a basis (up to torsion) for the Mordell–Weil group of an elliptic surface E → S where S is isomorphic to the projective line.

[Dandelin spheres](https://en.wikipedia.org/wiki/Dandelin_spheres) - one or two spheres that are tangent both to a plane and to a cone that intersects the plane.

[Deltahedron](https://en.wikipedia.org/wiki/Deltahedron) - a polyhedron whose faces are all equilateral triangles.

[Fractal](https://en.wikipedia.org/wiki/Fractal) - a subset of a Euclidean space for which the Hausdorff dimension strictly exceeds the topological dimension.

[Gabriel's Horn](https://en.wikipedia.org/wiki/Gabriel%27s_Horn) - a geometric figure which has infinite surface area but finite volume.

[Golden angle](https://en.wikipedia.org/wiki/Golden_angle) - the smaller of the two angles created by sectioning the circumference of a circle according to the golden ratio; that is, into two arcs such that the ratio of the length of the larger arc to the length of the smaller arc is the same as the ratio of the full circumference to the length of the larger arc.

[Polyomino](https://en.wikipedia.org/wiki/Polyomino) - a plane geometric figure formed by joining one or more equal squares edge to edge. It is a polyform whose cells are squares. It may be regarded as a finite subset of the regular square tiling with a connected interior.

[Prince Rupert's Cube](https://en.m.wikipedia.org/wiki/Prince_Rupert%27s_cube) - is the largest cube that can pass through a hole cut through a unit cube, i.e. through a cube whose sides have length 1, without splitting the cube into two pieces.

[Pseudosphere](https://en.wikipedia.org/wiki/Pseudosphere) - a surface with constant negative Gaussian curvature.z

[Spherical polyhedron](https://en.wikipedia.org/wiki/Spherical_polyhedron) - a tiling of the sphere in which the surface is divided or partitioned by great arcs into bounded regions called spherical polygons.

[Sphericon](https://en.wikipedia.org/wiki/Sphericon) - a solid that has a continuous developable surface with two congruent semi circular edges, and four vertices that define a square.

[Tautochrone curve](https://en.wikipedia.org/wiki/Tautochrone_curve) - A tautochrone or isochrone curve is the curve for which the time taken by an object sliding without friction in uniform gravity to its lowest point is independent of its starting point.

[Toroid](https://en.wikipedia.org/wiki/Toroid) - a surface of revolution with a hole in the middle, like a doughnut, forming a solid body.

### Graph Theory

[Kirchhoff's theorem](https://en.wikipedia.org/wiki/Kirchhoff%27s_theorem) - a theorem about the number of spanning trees in a graph, showing that this number can be computed in polynomial time as the determinant of a matrix derived from the graph.

### Group Theory

[Monster group](https://en.wikipedia.org/wiki/Monster_group) - the largest sporadic simple group.

[Sporadic group](https://en.wikipedia.org/wiki/Sporadic_group) - a sporadic group is one of the 26 exceptional groups found in the classification of finite simple groups.

### Logic

[Constraint satisfaction problem](https://en.wikipedia.org/wiki/Constraint_satisfaction_problem) - mathematical questions defined as a set of objects whose state must satisfy a number of constraints or limitations.

[Decision problem](https://en.wikipedia.org/wiki/Decision_problem) - a decision problem is a problem that can be posed as a yes-no question of the input values.

[First-order logic](https://en.wikipedia.org/wiki/First-order_logic) - a collection of formal systems used in mathematics, philosophy, linguistics, and computer science.

[Non-interactive zero-knowledge proof](https://en.wikipedia.org/wiki/Non-interactive_zero-knowledge_proof) - a variant of zero-knowledge proofs in which no interaction is necessary between prover and verifier.

[Satisfiability modulo theories](https://en.wikipedia.org/wiki/Satisfiability_modulo_theories) - a decision problem for logical formulas with respect to combinations of background theories expressed in classical first-order logic with equality.

[Zero-knowledge proof](https://en.wikipedia.org/wiki/Zero-knowledge_proof) - a method by which one party (the prover) can prove to another party (the verifier) that they know a value x, without conveying any information apart from the fact that they know the value x.

### Miscellaneous

[Abuse of notation](https://en.wikipedia.org/wiki/Abuse_of_notation) - occurs when an author uses a mathematical notation in a way that is not formally correct but that seems likely to simplify the exposition or suggest the correct intuition (while being unlikely to introduce errors or cause confusion).

[Coin problem](https://en.wikipedia.org/wiki/Coin_problem) -  a mathematical problem that asks for the largest monetary amount that cannot be obtained using only coins of specified denominations. For example, the largest amount that cannot be obtained using only coins of 3 and 5 units is 7 units.

[Currying](https://en.wikipedia.org/wiki/Currying) - the technique of translating the evaluation of a function that takes multiple arguments into evaluating a sequence of functions, each with a single argument. For example, a function that takes two arguments, one from X and one from Y, and produces outputs in Z, by currying is translated into a function that takes a single argument from X and produces as outputs functions from Y to Z.

[Elo rating system](https://en.wikipedia.org/wiki/Elo_rating_system) - a method for calculating the relative skill levels of players in zero-sum games such as chess.

[Freshman's Dream](https://en.wikipedia.org/wiki/Freshman's_dream) - the erroneous equation (x + y)^n = x^n + y^n, where n is a real number (usually a positive integer greater than 1).

[Goat problem](https://en.wikipedia.org/wiki/Goat_problem) - How big must r be chosen in the diagram, in order for the red area to equal one half of the area of the circle? Illustration: A goat/bull/horse is tethered at point Q. How long needs the line to be, to allow the animal to graze on exactly one half of the circle area?

[Ham sandwich theorem](https://en.wikipedia.org/wiki/Ham_sandwich_theorem) - states that given n measurable "objects" in n-dimensional Euclidean space, it is possible to divide all of them in half (with respect to their measure, i.e. volume) with a single (n − 1)-dimensional hyperplane.

[List of Mathematical Jargon](https://en.wikipedia.org/wiki/List_of_mathematical_jargon)

[List of Unusual Units of Measurement](https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement)

[Moving sofa problem](https://en.wikipedia.org/wiki/Moving_sofa_problem) - a two-dimensional idealisation of real-life furniture-moving problems and asks for the rigid two-dimensional shape of largest area A that can be maneuvered through an L-shaped planar region with legs of unit width.

[Pancake Sorting](https://en.wikipedia.org/wiki/Pancake_sorting) - the colloquial term for the mathematical problem of sorting a disordered stack of pancakes in order of size when a spatula can be inserted at any point in the stack and used to flip all pancakes above it. A pancake number is the minimum number of flips required for a given number of pancakes.

[Penrose graphical notation](https://en.wikipedia.org/wiki/Penrose_graphical_notation) - a (usually handwritten) visual depiction of multilinear functions or tensors proposed by Roger Penrose in 1971.

[Ramsey Theory](https://en.wikipedia.org/wiki/Ramsey_theory) - named after the British mathematician and philosopher Frank P. Ramsey, is a branch of mathematics that studies the conditions under which order must appear. Problems in Ramsey theory typically ask a question of the form: "how many elements of some structure must there be to guarantee that a particular property will hold?" More specifically, Ron Graham describes Ramsey theory as a "branch of combinatorics".

[Recreational mathematics (Category)](https://en.wikipedia.org/wiki/Category:Recreational_mathematics) - Recreational mathematics is mathematics carried out for recreation (entertainment) rather than as a strictly research and application-based professional activity.

[Zero-sum game](https://en.wikipedia.org/wiki/Zero-sum_game) - a mathematical representation of a situation in which each participant's gain or loss of utility is exactly balanced by the losses or gains of the utility of the other participants.

### Numbers

[0.999....](https://en.wikipedia.org/wiki/0.999...) - denotes the repeating decimal consisting of infinitely many 9s after the decimal point (and one 0 before it), equal to 1.

[Golden ration](https://en.wikipedia.org/wiki/Golden_ratio) - two quantities are in the golden ratio if their ratio is the same as the ratio of their sum to the larger of the two quantities.

[Grandi's series](https://en.wikipedia.org/wiki/Grandi%27s_series) - the infinite series 1 − 1 + 1 − 1 + ⋯

[Happy number](https://en.wikipedia.org/wiki/Happy_number) - Starting with any positive integer, replace the number by the sum of the squares of its digits in base-ten, and repeat the process until the number either equals 1 (where it will stay), or it loops endlessly in a cycle that does not include 1. Those numbers for which this process ends in 1 are happy numbers, while those that do not end in 1 are unhappy numbers (or sad numbers).

[Illegal Number](https://en.wikipedia.org/wiki/Illegal_number) - An **illegal number** is a number that represents information which is illegal to possess, utter, propagate, or otherwise transmit in some legal jurisdictions.

[Kaprekar number](https://en.wikipedia.org/wiki/Kaprekar_number) - In mathematics, a non-negative integer is called a "Kaprekar number" for a given base if the representation of its square in that base can be split into two parts that add up to the original number, with the proviso that the part formed from the low-order digits of the square must be non-zero—although it is allowed to include leading zeroes. For instance, 45 is a Kaprekar number, because 452 = 2025 and 20 + 25 = 45. The number 1 is Kaprekar in every base, because 12 = 01   in any base, and   0 + 1 = 1.

[McNugget numbers](https://en.wikipedia.org/wiki/Coin_problem#McNugget_numbers) - A McNugget number is the total number of McDonald's Chicken McNuggets in any number of boxes. In the United Kingdom, the original boxes (prior to the introduction of the Happy Meal-sized nugget boxes) were of 6, 9, and 20 nuggets.

[Names of large numbers](https://en.wikipedia.org/wiki/Names_of_large_numbers)

[Number theory](https://en.wikipedia.org/wiki/Number_theory) - a branch of pure mathematics devoted primarily to the study of the integers.

[Plastic number](https://en.wikipedia.org/wiki/Plastic_number) - is a mathematical constant which is the unique real solution of the cubic equation

[Polite number](https://en.wikipedia.org/wiki/Polite_number) - a positive integer that can be written as the sum of two or more consecutive positive integers.

[Roman Numerals](https://en.wikipedia.org/wiki/Roman_numerals) - The numeric system represented by Roman numerals originated in ancient Rome and remained the usual way of writing numbers throughout Europe well into the Late Middle Ages.

[Silver ratio](https://en.wikipedia.org/wiki/Silver_ratio) - two quantities are in the silver ratio (also silver mean or silver constant) if the ratio of the sum of the smaller and twice the larger of those quantities, to the larger quantity, is the same as the ratio of the larger one to the smaller one.

## Odds and Ends

[Cold Reading](https://en.wikipedia.org/wiki/Cold_reading) - a set of techniques used by mentalists, psychics, fortune-tellers, mediums, illusionists (readers), and scam artists to imply that the reader knows much more about the person than the reader actually does.

[Curse of the Colonel](https://en.wikipedia.org/wiki/Curse_of_the_Colonel) - an urban legend regarding a reputed curse placed on the Japanese Kansai-based Hanshin Tigers baseball team by deceased KFC founder and mascot Colonel Harland Sanders.

[Disambiguation (disambiguation)](https://en.wikipedia.org/wiki/Disambiguation_(disambiguation))

[Dooring](https://en.wikipedia.org/wiki/Dooring) - a traffic collision in which a cyclist rides into a car door or is struck by a car door that was opened quickly without checking first for cyclists by using the side mirror and/or performing a proper shoulder check out and back.

[Friedman Unit](https://en.wikipedia.org/wiki/Friedman_Unit) - One Friedman Unit is equal to six months, specifically the "next six months", a period repeatedly declared by New York Times columnist Thomas Friedman to be the most critical of the then-ongoing Iraq War even though such pronouncements extended back over two and a half years.

[Ivan Renko](https://en.wikipedia.org/wiki/Ivan_Renko) - Ivan Renko was a fictitious Yugoslav basketball player created by Bobby Knight when he was the head coach for the Indiana University Hoosiers.

[Luddite fallacy](https://en.wikipedia.org/wiki/Technological_unemployment#The_Luddite_fallacy) - used to express the view that those concerned about long term technological unemployment are committing a fallacy, as they fail to account for compensation effects.

[Numerology](https://en.wikipedia.org/wiki/Numerology) - any belief in the divine or mystical relationship between a number and one or more coinciding events.

[Paul is dead](https://en.wikipedia.org/wiki/Paul_is_dead) - an urban legend and conspiracy theory alleging that Paul McCartney, of the English rock band the Beatles, died in November 1966 and was secretly replaced by a look-alike.

[Postdiction](https://en.wikipedia.org/wiki/Postdiction) -  In skepticism, it is considered an effect of hindsight bias that explains claimed predictions of significant events such as plane crashes and natural disasters.

[Schroedinger's cat in popular culture](https://en.wikipedia.org/wiki/Schr%C3%B6dinger's_cat_in_popular_culture)

[Therblig](https://en.wikipedia.org/wiki/Therblig) - are 18 kinds of elemental motions used in the study of motion economy in the workplace. A workplace task is analyzed by recording each of the therblig units for a process, with the results used for optimization of manual labour by eliminating unneeded movements.

### Galleries, Glossaries and Lists

[Glossary of manias](https://en.wikipedia.org/wiki/Glossary_of_manias)

[Glossary of philosophy](https://en.wikipedia.org/wiki/Glossary_of_philosophy)

[Glossary of rhetorical terms](https://en.wikipedia.org/wiki/Glossary_of_rhetorical_terms)

[Glossary of shapes with metaphorical names](https://en.wikipedia.org/wiki/Glossary_of_shapes_with_metaphorical_names)

[Gallery of sovereign state flags](https://en.wikipedia.org/wiki/Gallery_of_sovereign_state_flags)

[Glossary of vexillology](https://en.wikipedia.org/wiki/Glossary_of_vexillology)

[List of artificial objects on the Moon](https://en.wikipedia.org/wiki/List_of_artificial_objects_on_the_Moon)

[List of chemical compounds with unusual names](https://en.wikipedia.org/wiki/List_of_chemical_compounds_with_unusual_names)

[List of cognitive biases](https://en.wikipedia.org/wiki/List_of_cognitive_biases)

[List of collective nouns of animals](https://en.wikipedia.org/wiki/List_of_English_terms_of_venery,_by_animal)

[List of common misconceptions](https://en.wikipedia.org/wiki/List_of_common_misconceptions)

[List of deadliest animals to humans](https://en.wikipedia.org/wiki/List_of_deadliest_animals_to_humans)

[List of Disney attractions that were never built](https://en.wikipedia.org/wiki/List_of_Disney_attractions_that_were_never_built)

[List of English words containing Q not followed by U](https://en.wikipedia.org/wiki/List_of_English_words_containing_Q_not_followed_by_U)

[List of English words without rhymes](https://en.wikipedia.org/wiki/List_of_English_words_without_rhymes)

[List of eponymous laws](https://en.wikipedia.org/wiki/List_of_eponymous_laws)

[List of Internet top-level domains](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains)

[List of inventors killed by their own inventions](https://en.wikipedia.org/wiki/List_of_inventors_killed_by_their_own_inventions)

[List of Latin phrases](https://en.wikipedia.org/wiki/List_of_Latin_phrases_(full))

[List of list of lists](https://en.wikipedia.org/wiki/List_of_lists_of_lists)

[List of memory biases](https://en.wikipedia.org/wiki/List_of_memory_biases)

[List of numeral systems](https://en.wikipedia.org/wiki/List_of_numeral_systems)

[List of organisms named after famous people](https://en.wikipedia.org/wiki/List_of_organisms_named_after_famous_people)

[List of organisms named after the Harry Potter series](https://en.wikipedia.org/wiki/List_of_organisms_named_after_the_Harry_Potter_series)

[List of pasta](https://en.wikipedia.org/wiki/List_of_pasta)

[List of paradoxes](https://en.wikipedia.org/wiki/List_of_paradoxes)

[List of philosophies](https://en.wikipedia.org/wiki/List_of_philosophies)

[List of phobias](https://en.wikipedia.org/wiki/List_of_phobias)

[List of prime numbers](https://en.wikipedia.org/wiki/List_of_prime_numbers)

[List of selfie-related injuries and deaths](https://en.wikipedia.org/wiki/List_of_selfie-related_injuries_and_deaths)

[List of sexually active popes](https://en.wikipedia.org/wiki/List_of_sexually_active_popes)

[List of tallest structures in the world by country](https://en.wikipedia.org/wiki/List_of_tallest_structures_by_country)

[List of tautological place names](https://en.wikipedia.org/wiki/List_of_redundant_place_names)

[Wikipedia: Unusual articles](https://en.wikipedia.org/wiki/Wikipedia:Unusual_articles)

## Paradoxes

[Abilene Paradox](https://en.wikipedia.org/wiki/Abilene_paradox) - a group of people collectively decide on a course of action that is counter to the preferences of many or all of the individuals in the group.

[Coastline Paradox](https://en.wikipedia.org/wiki/Coastline_paradox) - counterintuitive observation that the coastline of a landmass does not have a well-defined length.

[Curry's paradox](https://en.wikipedia.org/wiki/Curry's_paradox) - a paradox in which an arbitrary claim F is proved from the mere existence of a sentence C that says of itself "If C, then F", requiring only a few apparently innocuous logical deduction rules. Since F is arbitrary, any logic having these rules proves everything.

[Zeno's Paradox](https://en.wikipedia.org/wiki/Zeno%27s_paradoxes) - Paradoxes about movement, one being the fact that to get halfway you first need to get halfway there, then halfway there, etc.

## Phenomena, Effects and Laws

[Apophenia](https://en.wikipedia.org/wiki/Apophenia) - the tendency to mistakenly perceive connections and meaning between unrelated things.

[Betteridge's Law of Headlines](https://en.wikipedia.org/wiki/Betteridge's_law_of_headlines) - "Any headline that ends in a question mark can be answered by the word no."

[Broken Windows Theory](https://en.wikipedia.org/wiki/Broken_windows_theory) - Criminological theory that visible signs of crime, anti-social behavior, and civil disorder create an urban environment that encourages further crime and disorder, including serious crimes.

[Computer says no](https://en.wikipedia.org/wiki/Computer_says_no) - Popular name given to an attitude seen in some public-facing organisations where the default response to a customer’s request is to check with information stored on or generated by a computer, and then make decisions based on that, often in the face of common sense.

[Crab mentality](https://en.wikipedia.org/wiki/Crab_mentality) - is a way of thinking best described by the phrase "if I can't have it, neither can you".

[Cunningham's Law](https://meta.wikimedia.org/wiki/Cunningham%27s_Law) - The best way to get the right answer on the internet is not to ask a question; it's to post the wrong answer.

[Dear enemy effect](https://en.wikipedia.org/wiki/Dear_enemy_effect) - n ethological phenomenon in which two neighbouring territorial animals become less aggressive toward one another once territorial borders are well-established.

[Droste Effect](https://en.wikipedia.org/wiki/Droste_effect) - Picture recursively appearing within itself

[Dutch disease](https://en.wikipedia.org/wiki/Dutch_disease) - the apparent causal relationship between the increase in the economic development of a specific sector (for example natural resources) and a decline in other sectors (like the manufacturing sector or agriculture).

[Half-life of knowledge](https://en.wikipedia.org/wiki/Half-life_of_knowledge) -  the amount of time that has to elapse before half of the knowledge or facts in a particular area is superseded or shown to be untrue.

[IKEA Effect](https://en.m.wikipedia.org/wiki/IKEA_effect) - The IKEA effect is a cognitive bias in which consumers place a disproportionately high value on products they partially created.

[Law of triviality](https://en.wikipedia.org/wiki/Law_of_triviality) - members of an organization give disproportionate weight to trivial issues.

[Littlewood's law](https://en.wikipedia.org/wiki/Littlewood%27s_law) - a person can expect to experience events with odds of one in a million (defined by the law as a "miracle") at the rate of about one per month.

[Matthew effect](https://en.wikipedia.org/wiki/Matthew_effect) - "the rich get richer and the poor get poorer".

[McCollough Effect](https://en.wikipedia.org/wiki/McCollough_effect) - The McCollough effect is a phenomenon of human visual perception in which colorless gratings appear colored contingent on the orientation of the gratings.

[Muphry's Law](https://en.wikipedia.org/wiki/Muphry%27s_law) - An adage that states: "If you write anything criticizing editing or proofreading, there will be a fault of some kind in what you have written."

[Nominative Determinism](https://en.wikipedia.org/wiki/Nominative_determinism) - the hypothesis that people tend to gravitate towards areas of work that fit their names.

[Pareidolia](https://en.wikipedia.org/wiki/Pareidolia) - the tendency to interpret a vague stimulus as something known to the observer, such as seeing shapes in clouds, seeing faces in inanimate objects or abstract patterns, or hearing hidden messages in music.

[Pareto Principle](https://en.wikipedia.org/wiki/Pareto_principle) - The Pareto principle (also known as the 80/20 rule, the law of the vital few, or the principle of factor sparsity) states that, for many events, roughly 80% of the effects come from 20% of the causes.

[Parkinson's law](https://en.wikipedia.org/wiki/Parkinson%27s_law) - the adage that "work expands so as to fill the time available for its completion".

[Poe's Law](https://en.wikipedia.org/wiki/Poe%27s_law) - Parody so extreme that you can't tell if it's sincere or sarcastic.

[Scully Effect](https://en.wikipedia.org/wiki/Dana_Scully#%22The_Scully_Effect%22) - The character is believed to have initiated a phenomenon referred to as "The Scully Effect"; as the medical doctor and the FBI Special Agent inspired many young women to pursue careers in science, medicine and law enforcement, and as a result brought a perceptible increase in the number of women in those fields.

[The Wrong Type of Snow](https://en.wikipedia.org/wiki/The_wrong_type_of_snow) - A British Rail press release implied that management and its engineering staff were unaware of different types of snow. Henceforth in the United Kingdom, the phrase became a byword for euphemistic and pointless excuses.

[Twinkie Defense](https://en.wikipedia.org/wiki/Twinkie_defense) - "Twinkie defense" is a derisive label for an improbable legal defense.

## Philosophy

[Absurdism](https://en.wikipedia.org/wiki/Absurdism) - In philosophy, "the Absurd" refers to the conflict between the human tendency to seek inherent value and meaning in life and the human inability to find any in a purposeless, meaningless or chaotic and irrational universe.

[Five-minute hypothesis](https://en.wikipedia.org/wiki/Omphalos_hypothesis#Five-minute_hypothesis) - a skeptical hypothesis put forth by the philosopher Bertrand Russell that proposes that the universe sprang into existence five minutes ago from nothing, with human memory and all other signs of history included.

[Hanlon's Razor](https://en.wikipedia.org/wiki/Hanlon%27s_razor) - Never attribute to malice that which is adequately explained by stupidity

[Homunculus Argument](https://en.wikipedia.org/wiki/Homunculus_argument) - The homunculus argument is a fallacy arising most commonly in the theory of vision. One may explain human vision by noting that light from the outside world forms an image on the retinas in the eyes and something (or someone) in the brain looks at these images as if they are images on a movie screen.

[Last Thursdayism](https://en.wikipedia.org/wiki/Omphalos_hypothesis#Last_Thursdayism) - "the world might as well have been created last Thursday".

[Omphalos Hypothesis](https://en.wikipedia.org/wiki/Omphalos_hypothesis) - The omphalos hypothesis is one attempt to reconcile the scientific evidence that the universe is billions of years old with the Genesis creation narrative, which implies that the Earth is only a few thousand years old.

[Russell's Teapot](https://en.wikipedia.org/wiki/Russell%27s_teapot) - an analogy to illustrate that the philosophic burden of proof lies upon a person making unfalsifiable claims, rather than shifting the burden of disproof to others. He wrote that if he were to assert, without offering proof, that a teapot, too small to be seen by telescopes, orbits the Sun somewhere in space between the Earth and Mars, he could not expect anyone to believe him solely because his assertion could not be proven wrong.

[Social Gadfly](https://en.wikipedia.org/wiki/Social_gadfly) -

## Puzzles/Thought Experiments

[Ant on a rubber rope](https://en.wikipedia.org/wiki/Ant_on_a_rubber_rope) - An ant starts to crawl along a taut rubber rope 1 km long at a speed of 1 cm per second (relative to the rubber it is crawling on). At the same time, the rope starts to stretch uniformly by 1 km per second, so that after 1 second it is 2  km long, after 2 seconds it is 3 km long, etc. Will the ant ever reach the end of the rope?

[Chicken or the egg](https://en.wikipedia.org/wiki/Chicken_or_the_egg) - The dilemma stems from the observation that all chickens hatch from eggs and all chicken eggs are laid by chickens. "Chicken-and-egg" is a metaphoric adjective describing situations where it is not clear which of two events should be considered the cause and which should be considered the effect, or to express a scenario of infinite regress, or to express the difficulty of sequencing actions where each seems to depend on others being done first.

[If a tree falls in a forest](https://en.wikipedia.org/wiki/If_a_tree_falls_in_a_forest) - a philosophical thought experiment that raises questions regarding observation and perception.

[Infinite Monkey Theorem](https://en.wikipedia.org/wiki/Infinite_monkey_theorem) - states that a monkey hitting keys at random on a typewriter keyboard for an infinite amount of time will almost surely type any given text, such as the complete works of William Shakespeare.

[Ship of Theseus](https://en.wikipedia.org/wiki/Ship_of_Theseus) - A thought experiment that raises the question of whether a ship—standing for an object in general—that has had all of its components replaced remains fundamentally the same object.

[The monkey and the coconuts](https://en.wikipedia.org/wiki/The_monkey_and_the_coconuts) - The monkey and the coconuts is a mathematical puzzle in the field of Diophantine analysis involving five sailors and a monkey on a desert island who divide up a pile of coconuts.

[Thompson's Lamp](https://en.wikipedia.org/wiki/Thomson%27s_lamp) - Thomson's lamp is a philosophical puzzle based on infinites. It was devised in 1954 by British philosopher James F. Thomson, who used it to analyze the possibility of a supertask, which is the completion of an infinite number of tasks.

## Science

### Astronomy

[Frame-dragging](https://en.wikipedia.org/wiki/Frame-dragging)

### Animals

[52-hertz whale](https://en.wikipedia.org/wiki/52-hertz_whale) - an individual whale of unidentified species, which calls at the very unusual frequency of 52 Hz.

[Chrismatic megafauna](https://en.wikipedia.org/wiki/Charismatic_megafauna) - large animal species with symbolic value or widespread popular appeal, and are often used by environmental activists to achieve environmentalist goals.

[Mike the Headless Chicken](https://en.wikipedia.org/wiki/Mike_the_Headless_Chicken) - a Wyandotte chicken that lived for 18 months after his head had been cut off.

[Potoooooooo](https://en.wikipedia.org/wiki/Potoooooooo) - an 18th-century Thoroughbred racehorse who won over 30 races and defeated some of the greatest racehorses of the time. He went on to be a sire. He is now best known for the unusual spelling of his name, pronounced Potatoes.

[Puppy cat](https://en.wikipedia.org/wiki/Puppy_cat) - a term used to refer to specific breeds of domestic cats that have unusual behavioral tendencies that are reminiscent of young domestic dogs.

[Raining of animals](https://en.wikipedia.org/wiki/Rain_of_animals) - a rare meteorological phenomenon in which flightless animals fall from the sky.

[Rat King](https://en.wikipedia.org/wiki/Rat_king) - A collection of rats whose tails are intertwined and bound together by one of several possible mechanisms, such as entangling material like hair or sticky substances like sap or gum.

[Sentinel Species](https://en.wikipedia.org/wiki/Sentinel_species) - are organisms, often animals, used to detect risks to humans by providing advance warning of a danger.

### Cartography, Geography

[Satellite map images with missing or unclear data](https://en.wikipedia.org/wiki/Satellite_map_images_with_missing_or_unclear_data)

[Trap street](https://en.wikipedia.org/wiki/Trap_street) -  a fictitious entry in the form of a misrepresented street on a map, often outside the area the map nominally covers, for the purpose of "trapping" potential copyright violators of the map who, if caught, would be unable to explain the inclusion of the "trap street" on their map as innocent.

### Fluid Dynamics

[Vortex Shedding](https://en.wikipedia.org/wiki/Vortex_shedding) - In fluid dynamics, vortex shedding is an oscillating flow that takes place when a fluid such as air or water flows past a bluff (as opposed to streamlined) body at certain velocities, depending on the size and shape of the body.

### Physics

[Eddy current](https://en.wikipedia.org/wiki/Eddy_current) -  Loops of electrical current induced within conductors by a changing magnetic field in the conductor according to Faraday's law of induction.

[Faraday Cage](https://en.wikipedia.org/wiki/Faraday_cage) - An enclosure used to block electromagnetic fields.

### Psychology

[Confirmation bias](https://en.wikipedia.org/wiki/Confirmation_bias) - the tendency to search for, interpret, favor, and recall information in a way that confirms one's preexisting beliefs or hypotheses.

[Rosenhan Experiment](https://en.wikipedia.org/wiki/Rosenhan_experiment) - An experiment conducted to determine the validity of psychiatric diagnosis. The experimenters feigned hallucinations to enter psychiatric hospitals, and acted normally afterwards.

[Selection Bias](https://en.wikipedia.org/wiki/Selection_bias) - the bias introduced by the selection of individuals, groups or data for analysis in such a way that proper randomization is not achieved, thereby ensuring that the sample obtained is not representative of the population intended to be analyzed.

[Semantic satiation](https://en.wikipedia.org/wiki/Semantic_satiation) - a psychological phenomenon in which repetition causes a word or phrase to temporarily lose meaning for the listener, who then perceives the speech as repeated meaningless sounds.

[Sensory-specific Satiety](https://en.wikipedia.org/wiki/Sensory-specific_satiety) - a sensory hedonic phenomenon that refers to the declining satisfaction generated by the consumption of a certain type of food, and the consequent renewal in appetite resulting from the exposure to a new flavor or food.

## Things

[Adventure playground](https://en.wikipedia.org/wiki/Adventure_playground) - a specific type of playground for children. Adventure playgrounds can take many forms, ranging from "natural playgrounds" to "junk playgrounds", and are typically defined by an ethos of unrestricted play, the presence of playworkers (or "wardens"), and the absence of adult-manufactured or rigid play-structures.

[Ampelmaennchen](https://en.wikipedia.org/wiki/Ampelm%C3%A4nnchen) - is the symbol shown on pedestrian signals in Germany.

[Archimedes' Screw](https://en.wikipedia.org/wiki/Archimedes'_screw) -  is a machine used for transferring water from a low-lying body of water into irrigation ditches.

[Christmas Bullet](https://en.wikipedia.org/wiki/Christmas_Bullet) - an American single-seat cantilever wing biplane. It is considered by many to be among the worst aircraft ever constructed.

[Clock of the Long Now](https://en.wikipedia.org/wiki/Clock_of_the_Long_Now) - a mechanical clock under construction, that is designed to keep time for 10,000 years.

[Cold War playground equipment](https://en.wikipedia.org/wiki/Cold_War_playground_equipment) - was intended to foster children's curiosity and excitement about the Space Race. It was installed during the Cold War in both communist and capitalist countries.

[Color trade mark](https://en.wikipedia.org/wiki/Colour_trade_mark) - a non-conventional trade mark where at least one colour is used to perform the trade mark function of uniquely identifying the commercial origin of products or services.

[Chicken gun](https://en.wikipedia.org/wiki/Chicken_gun) - a large-diameter, compressed-air cannon used to fire dead chickens at aircraft components in order to simulate high-speed bird strikes during the aircraft's flight.

[Crinkle crankle wall](https://en.wikipedia.org/wiki/Crinkle_crankle_wall) - The crinkle crankle wall economizes on bricks, despite its sinuous configuration, because it can be made just one brick thin. If a wall this thin were to be made in a straight line, without buttresses, it would easily topple over. The alternate convex and concave curves in the wall provide stability and help it to resist lateral forces.

[Czech Hedgehog](https://en.wikipedia.org/wiki/Czech_hedgehog) - The Czech hedgehog is a static anti-tank obstacle defense made of metal angle beams or I-beams (that is, lengths with an L- or I-shaped cross section).

[Donkey vote](https://en.wikipedia.org/wiki/Donkey_vote) - a ballot cast in an election that uses a preference voting system, where a voter is permitted or required to rank candidates on the ballot paper, and ranks them based on the order they appear on the ballot paper.

[Dual-tone multi-frequency signaling](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling) - a telecommunication signaling system using the voice-frequency band over telephone lines between telephone equipment and other communications devices and switching centers.

[Ersatz Good](https://en.wikipedia.org/wiki/Ersatz_good) - Substitute good, normally inferior.

[Fata Morgana](https://en.wikipedia.org/wiki/Fata_Morgana_(mirage)) - a complex form of superior mirage that is seen in a narrow band right above the horizon.

[Garden hermit](https://en.wikipedia.org/wiki/Garden_hermit) - hermits encouraged to live in purpose-built hermitages, follies, grottoes, or rockeries on the estates of wealthy land-owners, primarily during the 18th century. Such hermits would be encouraged to dress like druids and remain permanently on-site, where they could be fed, cared-for and consulted for advice or viewed for entertainment.

[Immovable ladder](https://en.wikipedia.org/wiki/Status_Quo_(Jerusalem_and_Bethlehem)#Immovable_ladder) - Some consider the so-called immovable ladder[c] under the window of the Church of the Holy Sepulchre to be a visible symbol of the alleged inactivity the Status Quo imposes.

[Lost work](https://en.wikipedia.org/wiki/Lost_work) - a document, literary work, or piece of multimedia produced some time in the past, of which no surviving copies are known to exist.

[Matrix digital rain](https://en.wikipedia.org/wiki/Matrix_digital_rain) - the computer code featured in the Matrix series. The falling green code is a way of representing the activity of the virtual reality environment of the Matrix on screen.

[Smoot](https://en.wikipedia.org/wiki/Smoot) - a nonstandard, humorous unit of length created as part of an MIT fraternity prank. It is named after Oliver R. Smoot, a fraternity pledge to Lambda Chi Alpha, who in October 1958 lay down repeatedly on the Harvard Bridge (between Boston and Cambridge, Massachusetts) so that his fraternity brothers could use his height to measure the length of the bridge.

[The Letters of Utrecht](https://en.wikipedia.org/wiki/The_Letters_of_Utrecht) - form an endless poem in the stones of a street in the center of the Dutch city of Utrecht.

### Food

[Soldiers (food)](https://en.wikipedia.org/wiki/Soldiers_(food)) - a thin strip of toast; the strips that a slice is cut into are reminiscent of soldiers on parade.

[World's longest hot dog](https://en.wikipedia.org/wiki/World's_longest_hot_dog)
